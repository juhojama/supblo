Simple PoC blockchain implementation.

Run the program with PHP:
`php App.php`

Example program output:
```
Array
(
    [0] => Supblo\Block Object
        (
            [index:Supblo\Block:private] => 0
            [previousHash:Supblo\Block:private] => 0
            [timestamp:Supblo\Block:private] => 1659369969
            [data:Supblo\Block:private] => stdClass Object
                (
                    [content] => genesis block
                )

            [hash:Supblo\Block:private] => d235f21778d67fc911e3a5f13b0427f0819298abd6dc3c5f1acd0189dcac84d8
        )

    [1] => Supblo\Block Object
        (
            [index:Supblo\Block:private] => 1
            [previousHash:Supblo\Block:private] => d235f21778d67fc911e3a5f13b0427f0819298abd6dc3c5f1acd0189dcac84d8
            [timestamp:Supblo\Block:private] => 1659369969
            [data:Supblo\Block:private] => stdClass Object
                (
                    [content] => This is the second block
                )

            [hash:Supblo\Block:private] => 9286083b605faf95cb660233d39cca044621cb13b2daaec38e676e1e788df22a
        )

)
```

