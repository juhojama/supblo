<?php

namespace Supblo;

use stdClass;
use Supblo\Blockchain;

class Block
{
    public function __construct(private int $index, private string $previousHash, private int $timestamp, private object $data, private string $hash)
    {
    }

    public static function calculateHash(int $index, string $previousHash, int $timestamp, object $data)
    {
        return hash('sha256', strval($index) . $previousHash . strval($timestamp) . json_encode($data));
    }

    public function generateNextBlock(object $blockData)
    {
        $nextIndex = $this->index + 1;
        $nextTimestamp = time();
        $nextHash = self::calculateHash(index: $nextIndex, previousHash: $this->hash, timestamp: $nextTimestamp, data: $blockData);
        return new self(
            index: $nextIndex,
            previousHash: $this->hash,
            timestamp: $nextTimestamp,
            data: $blockData,
            hash: $nextHash
        );
    }

    public static function getGenesisBlock()
    {
        $timestamp = time();
        $hash = self::calculateHash(index: 0, previousHash: '0', timestamp: $timestamp, data: (object)['content' => 'genesis block']);
        return new self(index: 0, previousHash: '0', timestamp: $timestamp, data: (object)['content' => 'genesis block'], hash: $hash);
    }

    public static function isValidNewBlock(Block $newBlock, Block $previousBlock): bool
    {
        if ($previousBlock->getIndex() + 1 !== $newBlock->getIndex()) {
            return false;
        } elseif ($previousBlock->getHash() !== $newBlock->getPreviousHash()) {
            // throw new HashNoMatchException();
            return false;
        } elseif (self::calculateHash(index: $newBlock->getIndex(), previousHash: $newBlock->getPreviousHash(), timestamp: $newBlock->getTimestamp(), data: $newBlock->getData()) !== $newBlock->getHash()) {
            return false;
        }
        return true;
    }

    /**
     * Validates blockchain.
     *
     * @param Blockchain $blockchain 
     * @return void 
     */
    public static function isValidChain(Blockchain $blockchain)
    {
        if (json_encode(self::getGenesisBlock()) !== json_encode($blockchain[0])) {
            return false;
        }
        $tempBlocks = [$blockchain->getBlocks()[0]];

        for ($i = 1; $i < count($blockchain->getBlocks()); $i++) {
            if (self::isValidNewBlock($blockchain->getBlocks()[$i], $tempBlocks[$i - 1])) {
                array_push($tempBlocks, $blockchain->getBlocks()[$i]);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the value of index
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Get the value of hash
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Get the value of previousHash
     */
    public function getPreviousHash()
    {
        return $this->previousHash;
    }

    /**
     * Get the value of timestamp
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Get the value of data
     */
    public function getData()
    {
        return $this->data;
    }
}
