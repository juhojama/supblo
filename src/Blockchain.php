<?php

namespace Supblo;

use stdClass;

/**
 * Class to contain blocks of blockchain. Singleton pattern used.
 * @package Supblo
 */
class Blockchain
{
    private static ?self $instance = null;
    private static ?array $blocks = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Adds block to chain.
     *
     * @param Block $block 
     * @return void 
     */
    public function addBlock(Block $block): void
    {
        if ($this->blocks === null) {
            $this->blocks = [$block];
            return;
        }
        if (Block::isValidNewBlock($block, $this->getLatestBlock())) {
            array_push($this->blocks, $block);
        }
    }

    public function replaceChain(Blockchain $newBlockchain)
    {
        $blockchain = Blockchain::getInstance();
        if (Block::isValidChain($newBlockchain) && count($newBlockchain->getBlocks()) > count($blockchain->getBlocks())) {
            // Replace blockchain with new blocks
            $this->blocks = $newBlockchain->getBlocks();
        } else {
            // Invalid newBlocks
            var_dump('Invalid new blockchain');
        }
    }

    public function getLatestBlock(): Block
    {
        return $this->blocks[count($this->blocks) - 1];
    }

    /**
     * Get the value of blocks
     */
    public function getBlocks(): ?array
    {
        return $this->blocks;
    }
}
