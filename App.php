<?php

use Supblo\Block;
use Supblo\Blockchain;

require_once 'vendor/autoload.php';

// Initialise blockchain with a genesis block
$blockchain = Blockchain::getInstance();
$blockchain->addBlock(Block::getGenesisBlock());
$blockchain->addBlock(($blockchain->getLatestBlock())->generateNextBlock((object)['content' => 'This is the second block']));

print_r($blockchain->getBlocks());